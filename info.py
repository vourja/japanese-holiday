# package information.
INFO = dict(
  name        = "japanese_holiday",
  description = "Get Japanese holiday lib.",
  author      = "J Yukawa",
  author_email = "j.yukawa@gmail.com",
  license     = "MIT",
  url         = "https://bitbucket.org/vourja/japanese-holiday",
  classifiers = [
    "Development Status :: 1 - Planning",
    "Intended Audience :: Developers",
    "Programming Language :: Python",
    "Programming Language :: Python :: 2.7",
    "License :: OSI Approved :: MIT License"
  ],
  keywords='japanese holiday',

)
